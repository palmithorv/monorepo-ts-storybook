import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import * as React from 'react';
import { d } from '@commons/d';
import { B } from '@commons/b';

storiesOf('@commons/b/1. Basic', module).add(
  'first',
  withInfo({ inline: false })(() => {
    return <B>{d()}</B>;
  })
);
